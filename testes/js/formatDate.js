// Escreva uma função que converta a data de entrada do usuário formatada como MM/DD/YYYY em um formato exigido por uma API (YYYYMMDD). O parâmetro "userDate" e o valor de retorno são strings.

// Por exemplo, ele deve converter a data de entrada do usuário "31/12/2014" em "20141231" adequada para a API.


function formatDate(userDate) {
  let group = userDate.match(/(\d{2})\/(\d{2})\/(\d{4})/);
  if (!group) throw "Data em formato invalido";
  let [ mes, dia, ano ] = group.slice(1, 4);
  return `${ano}${mes}${dia}`;
  // format from M/D/YYYY to YYYYMMDD
}

console.log(formatDate("12/1/2014"));

// let string = '12/31/2014'

// console.log( mes, dia, ano )
