<?php


/* ----------------- DESCRIÇÃO DO TESTE -----------------------*/

/*
Implemente uma função que ao receber um array associativo contendo arquivos e donos, retorne os arquivos agrupados por dono. 

Por exempolo, um array ["Input.txt" => "Jose", "Code.py" => "Joao", "Output.txt" => "Jose"] a função groupByOwners deveria retornar ["Jose" => ["Input.txt", "Output.txt"], "Joao" => ["Code.py"]].

*/

class FileOwners
{
    public static function groupByOwners($files)
    {
        $owners = array();
        foreach($files as $key => $value){
          if(!isset($owners[$value]) or !is_array($owners[$value])){
            $owners[$value] = [];
          }
          array_push($owners[$value], $key);
        }
        return $owners;
    }
}

/*
  Como a lógica de atribuição não especifica se a chave deve ser case insensitive, não o implementei.
  Nomes diferentes resultarão em chaves diferentes.
*/
$files = array
(
    "Input.txt" => "Jose",
    "Code.py" => "Joao",
    "Output.txt" => "Jose",
    "Click.js" => "Maria",
    "Out.php" => "maria",
);


var_dump(FileOwners::groupByOwners($files));

